
Overview:
--------
This module adds a "Search recent log" link in the Reports section of the
adminstration pages. Users can search by message type, message, user, time
or hostname. Useful for figuring out how many posts you got in a day, how
many logins in a week, errors from a certain user, etc.

Only visible to users with the "access site reports" privilege.

Installation and configuration:
------------------------------
Installation is as simple as copying the module into your modules
directory, then enabling the module at
  Administer >> Site Building >> Modules

Go to
  Administer >> Reports

and you'll see a "Search recent log" link.